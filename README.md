Introdizer
============
This will write the _introduction_ for your essay or question given a topic.

Built with
============
Selenium - Framework used to scour the web

Versioning
============
I use  [SemVer](http://semver.org/) for versioning.

Author
=======
Shaheen Sarafa

License
========
Introdizer is licensed under GPU GPLv3 license. see the [LICENSE](LICENSE) file for details.

Acknowledgments
==================
* Farrah Jaber, my friend, for the inspiration.
