"""
    Introdizer will write the introduction for your essay or research question
    Copyright (C) 2019  Shaheen Sarafa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from os.path import abspath
from internet_scraper import InternetScraper
from rewriter import Rewriter


def genre_check(topic: str):
    """This tries to guess the genre of the topic"""
    # Change what's below to use machine learning
    # Use scilearn to classify a bunch of essay questions
    comparision_words = ["Comparitive", ["difference", "between"], "comparision", "compare"]
    pros_cons_words = ["Pros and cons", "advantages", "disadvantages", "pros", "cons"]
    # descriptive_words = ["Descriptive", ["how", "works"], ["what", "is"]]
    genres = [comparision_words, pros_cons_words]
    for genre in genres:
        for tag in genre:
            if isinstance(tag, list):
                for element in tag:
                    all_found = True if str(element) in topic else False
                if all_found:
                    topic_genre = genre
            elif tag in topic:
                topic_genre = genre
    return topic_genre

def rewrite(paragraph):
    """Function in charge of handling rewriting the chosen paragraph"""

    rewriter = Rewriter(abspath("geckodriver"), paragraph)

    rewriter.input_paragraph()
    rewriter.bypass_question()
    rewriter.submit_request()

    return rewriter.get_result()

def run():
    """Main function for the program"""

    topic = input("What is the main topic? ").lower()
    print("Preparing an {0} essay".format(genre_check(topic)[0]))

    geckodriver = abspath("geckodriver")
    reader = InternetScraper(geckodriver, topic)

    reader.initial_search()
    reader.site_visitor()
    reader.display_introductions()

    paragraph_choice = int(input("Which paragraph do you want to rewrite"))
    paragraph_choice = reader.introductions[paragraph_choice-1]

    print(rewrite(paragraph_choice))

if __name__ == "__main__":
    run()
    pass
